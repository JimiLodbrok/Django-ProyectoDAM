from django.db import models
from equipos.models import Equipo
from jugadores.models import Jugador

# Create your models here.

class Partido(models.Model):
    puntos_local = models.IntegerField(default=0)
    puntos_visitante = models.IntegerField(default=0)
    equipo_local = models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name='equipo_local')
    equipo_visitante = models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name='equipo_visitante')
    mvp = models.ForeignKey(Jugador, on_delete=models.CASCADE, related_name='jugador_mvp', blank=True, null=True)
    fecha = models.DateField(auto_now_add=True)

    def __str__(self):
        return '{} vs {}'.format(self.equipo_local, self.equipo_visitante)

    class Meta:
        ordering = ('id',)
        verbose_name_plural = 'Partidos'
        unique_together = ('equipo_local', 'equipo_visitante')
