from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from django.contrib import messages
from django.db import IntegrityError

from .forms import PartidoForm
from .models import Partido
# Create your views here.

class PartidoCreate(CreateView):
    model = Partido
    form_class = PartidoForm
    template_name = 'partido_create.html'
    success_url = reverse_lazy('equipos:list')

    def get_context_data(self, **kwargs):
        title = "Nuevo Partido"
        context = super().get_context_data(**kwargs)
        context['title'] = title
        return context