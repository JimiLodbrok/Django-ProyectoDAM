from django.contrib import admin
from .models import Partido

# Register your models here.
@admin.register(Partido)
class AdminPartido(admin.ModelAdmin):
    list_display = ('__str__', 'fecha')
    list_filter = ('equipo_local', 'equipo_visitante', 'fecha',)
