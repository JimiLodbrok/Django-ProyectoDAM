from django import forms
from .models import Partido
from material import *

class PartidoForm(forms.ModelForm):
    layout = Layout(Row('equipo_local', 'equipo_visitante'),
                    Row('puntos_local', 'puntos_visitante'),
                    'mvp'
                    )
    class Meta:
        model = Partido
        fields = '__all__'
        #labels = {'nombre': Nombre}  sirve para poner el titulo de cada input del formulario
        #widgets = {'nombre': forms.textInput(attrs={'class': 'form-control'}), forms.Select().....} para definir el tipo de input