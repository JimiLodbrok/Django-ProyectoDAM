# Generated by Django 2.0.4 on 2018-06-02 16:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('partidos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partido',
            name='equipo_local',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='equipo_local', to='equipos.Equipo'),
        ),
        migrations.AlterField(
            model_name='partido',
            name='equipo_visitante',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='equipo_visitante', to='equipos.Equipo'),
        ),
        migrations.AlterField(
            model_name='partido',
            name='mvp',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='jugador_mvp', to='jugadores.Jugador'),
        ),
    ]
