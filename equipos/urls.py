'''Equipo Configuration'''
from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    path('', login_required(views.EquipoList.as_view()), name='list'),
    path('new', login_required(views.EquipoCreate.as_view()), name="new"),
    path('<pk>', login_required(views.EquipoDetail.as_view()), name="detail"),
    path('update/<pk>', login_required(views.EquipoUpdate.as_view()), name="update"),
    path('delete/<pk>', login_required(views.EquipoDelete.as_view()), name="delete"),
    path('baja/<pk>/<id_equipo>', login_required(views.update_db_field), name="quitar"),
]
