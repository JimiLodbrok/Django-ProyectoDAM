from django.contrib import admin
from .models import Equipo

# Register your models here.
@admin.register(Equipo)
class AdminEquipo(admin.ModelAdmin):
    list_display = ('nombre', 'localidad', 'campo',)
    lis_filter = ('localidad')
