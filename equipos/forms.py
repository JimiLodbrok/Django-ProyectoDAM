from django import forms
from .models import Equipo
from material import *

class EquipoForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = '__all__'
        #labels = {'nombre': Nombre}  sirve para poner el titulo de cada input del formulario
        #widgets = {'nombre': forms.textInput(attrs={'class': 'form-control'}), forms.Select().....} para definir el tipo de input