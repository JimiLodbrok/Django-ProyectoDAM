from django.db import models

# Create your models here.

class Equipo(models.Model):
    '''Prueba modelo'''
    nombre = models.CharField(max_length=50)
    localidad = models.CharField(max_length=25)
    web = models.CharField(max_length=50, blank=True)
    campo = models.CharField(max_length=50, blank=True)
    image = models.ImageField(null=True, blank=True, default='equipo_default.jpg')

    def __str__(self):
        return self.nombre
        
    class Meta:
        ordering = ('id',)
        