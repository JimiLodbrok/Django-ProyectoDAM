import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from .forms import EquipoForm
from .models import Equipo

from django.core import serializers

from jugadores.models import Jugador

# Create your views here.
'''
def serializar_equipo(request):
    data = serializers.serialize('json', Equipo.objects.all())
    return HttpResponse(data, content_type='application/json')
'''
class EquipoList(ListView):
    model = Equipo
    template_name = 'equipo_list.html'

    def get_context_data(self, **kwargs):
        title = "Administración de Equipos"
        context = super().get_context_data(**kwargs)
        data = serializers.serialize('json', Equipo.objects.all())       
        context['equipos'] = data
        context['title'] = title
        return context

class EquipoDetail(DetailView):
    model = Equipo
    template_name = 'equipo_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        title = "Plantilla {}".format(context['object'].nombre) 
        jugadores = Jugador.objects.filter(equipo=context['object'].id)
        context['title'] = title
        context['jugadores'] = jugadores
        return context

class EquipoCreate(CreateView):
    model = Equipo
    form_class = EquipoForm
    template_name = 'equipo_create.html'
    success_url = reverse_lazy('equipos:list')

    def get_context_data(self, **kwargs):
        title = "Nuevo Equipo"
        context = super().get_context_data(**kwargs)
        context['title'] = title
        return context

class EquipoUpdate(UpdateView):
    model = Equipo
    form_class = EquipoForm
    template_name = 'equipo_create.html'
    success_url = reverse_lazy('equipos:list')

    def get_context_data(self, **kwargs):
        title = "Editar Equipo"
        context = super().get_context_data(**kwargs)
        context['title'] = title
        return context

class EquipoDelete(DeleteView):
    model = Equipo
    success_url = reverse_lazy('equipos:list')
    
    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

def update_db_field(request,pk,id_equipo):
    jugador = Jugador.objects.get(pk=pk)
    jugador.equipo = None
    jugador.save()
    return HttpResponseRedirect('/equipos/' + id_equipo)
