'''Jugador Configuration'''
from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views


urlpatterns = [
    path('', views.JugadorList.as_view(), name='list'),
    path('new', login_required(views.JugadorCreate.as_view()), name="new"),
    path('<pk>', login_required(views.JugadorDetail.as_view()), name="detail"),
    path('update/<pk>', views.JugadorUpdate.as_view(), name="update"),
    path('delete/<pk>', views.JugadorDelete.as_view(), name="delete"),
    path('fichaje/<pk>/', login_required(views.update_db_field), name="fichar"),
]