import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from .models import Jugador, Posicion
from stats.models import Estadistica
from equipos.models import Equipo

from .serializers import EstadisticasSerializer

from django.core import serializers

from .forms import JugadorForm

# Create your views here.   

class JugadorList(ListView):
    model = Jugador
    template_name = 'jugador_list.html'

    def get_context_data(self, **kwargs):
        title = "Administración de Jugadores"
        context = super().get_context_data(**kwargs)
        data = serializers.serialize('json', Jugador.objects.all())       
        context['equipos'] = data
        context['title'] = title
        return context

class JugadorDetail(DetailView):
    model = Jugador
    template_name = 'jugador_detail.html'
    context_object_name = 'jugador'

    def get_context_data(self, **kwargs):
        title = "Detalle Jugador"
        context = super().get_context_data(**kwargs) 

        stats = Estadistica.objects.filter(jugador=context['object'].id)
        estadisticas = serializers.serialize('json', stats)

        lista_partidos = [{
            'local': partido.partido.equipo_local,
            'visitante': partido.partido.equipo_visitante,
            'puntosLocal': partido.partido.puntos_local,
            'puntosVisitante': partido.partido.puntos_visitante,
            'puntos': partido.puntos,
            'asistencias': partido.asistencias,
            'rebotes': partido.rebotes,
            't2m': partido.t2m,
            't2f': partido.t2f,
            't3m': partido.t3m,
            't3f': partido.t3f,
            'tlm': partido.tlm,
            'tlf': partido.tlf,
            } for partido in stats]    
        
        lista_equipos = [{
            "local": equipo['local'].nombre,
            "visitante": equipo['visitante'].nombre,
            "puntosLocal": equipo['puntosLocal'],
            "puntosVisitante": equipo['puntosVisitante'],
            "puntos": equipo['puntos'],
            "asistencias": equipo['asistencias'],
            "rebotes": equipo['rebotes'],
            't2m': equipo['t2m'],
            't2f': equipo['t2f'],
            't3m': equipo['t3m'],
            't3f': equipo['t3f'],
            'tlm': equipo['tlm'],
            'tlf': equipo['tlf'],
            }for equipo in lista_partidos]


        lista_equipos = json.dumps(lista_equipos, ensure_ascii=False)
        
        print(lista_equipos)

        context['title'] = title
        context['estadisticas'] = estadisticas
        context['lista_equipos'] = lista_equipos
        return context

class JugadorCreate(CreateView):
    model = Jugador
    form_class = JugadorForm
    template_name = 'jugador_create.html'
    success_url = reverse_lazy('jugadores:list')

    def get_context_data(self, **kwargs):
        title = "Nuevo Jugador"
        context = super().get_context_data(**kwargs)
        context['title'] = title
        return context

class JugadorUpdate(UpdateView):
    model = Jugador
    form_class = JugadorForm
    template_name = 'jugador_create.html'
    success_url = reverse_lazy('jugadores:list')

    def get_context_data(self, **kwargs):
        title = "Editar Jugador"
        context = super().get_context_data(**kwargs)
        context['title'] = title
        return context

class JugadorDelete(DeleteView):
    model = Jugador
    success_url = reverse_lazy('jugadores:list')
    
    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

def update_db_field(request,pk):
    jugador = Jugador.objects.get(pk=pk)
    jugador.equipo = Equipo.objects.get(pk=1)
    jugador.save()
    return HttpResponseRedirect('/equipos/1')