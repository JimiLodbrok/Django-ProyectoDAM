from django.contrib import admin
from .models import Jugador, Posicion

# Register your models here.
@admin.register(Jugador)
class AdminEquipo(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'altura', 'posicion', 'rol')
    list_filter = ('posicion', 'rol',)

@admin.register(Posicion)
class AdminPosicion(admin.ModelAdmin):
    list_display = ('posicion',)
    list_filter = ('posicion',)
