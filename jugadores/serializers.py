from rest_framework.serializers import ModelSerializer

from stats.models import Estadistica

class EstadisticasSerializer(ModelSerializer):
    model = Estadistica
    fields = '__all__'