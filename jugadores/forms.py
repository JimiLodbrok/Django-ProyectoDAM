from django import forms
from .models import Jugador
from material import *

class JugadorForm(forms.ModelForm):
    layout = Layout('nombre', 'apellido',
                    Row('altura', 'peso', 'fnacimiento'),
                    'image', 'equipo',
                    Row('dorsal', 'posicion', 'rol'))
    class Meta:
        model = Jugador
        fields = '__all__'
        #labels = {'nombre': Nombre}  sirve para poner el titulo de cada input del formulario
        #widgets = {'nombre': forms.textInput(attrs={'class': 'form-control'}), forms.Select().....} para definir el tipo de input
                   