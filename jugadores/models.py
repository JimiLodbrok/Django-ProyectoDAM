from django.db import models
from equipos.models import Equipo
from .utils import ChoiceEnum

# Create your models here.

class Posicion(models.Model):
    posicion = models.CharField(max_length=25)

    def __str__(self):
        return '{}'.format(self.posicion)

    class Meta:
        ordering = ('id',)
        verbose_name_plural = 'Posiciones'

class Jugador(models.Model):
    class Rols(ChoiceEnum):
        DEF = 'Defensor'
        TAP = 'Taponador'
        ATL = 'Atlético'
        ANOT = 'Anotador'
        PN = 'Penetrador'
        HC = 'Hombre clave'
        PR = 'Promesa'
        H6 = 'Sexto hombre'
        SUPL = 'Suplente'
        TMATE = 'Jugador de equipo'
        NOV = 'Novato'

    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    altura = models.DecimalField(max_digits=3, decimal_places=2)
    peso = models.DecimalField(max_digits=3, decimal_places=1)
    dorsal = models.IntegerField(default=0, blank=True)
    image = models.ImageField(blank=True, default='player.jpg')
    fnacimiento = models.DateField(null=True, blank=True)
    equipo = models.ForeignKey(Equipo, null=True, blank=True, on_delete=models.SET_NULL)
    funion = models.DateField(auto_now_add=True, null=True, blank=True)
    posicion = models.ForeignKey(Posicion, null=True, blank=True, on_delete=models.SET_NULL)
    rol = models.CharField(max_length=6, choices=Rols.choices(), blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellido)

    class Meta:
        ordering = ('id',)
        verbose_name_plural = 'Jugadores'
