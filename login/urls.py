'''Login Configuration'''
from django.urls import path, re_path, include
from django.conf.urls import url
from django.contrib.auth.views import login, logout_then_login
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete
from . import views

urlpatterns = [
    path('', login, {'template_name':'index.html'}, name='login'),
    path('registrar/', views.RegistroUsuario.as_view(), name='registrar'),
    path('logout/', logout_then_login, name='logout'),
    path('reset/password_reset', password_reset, {'template_name':'registration/password_reset_form.html',
        'email_template_name':'registration/password_reset_email.html', 'post_reset_redirect':'login:password_reset_done'}, name='password_reset'),    
    path('reset/password_reset_done', password_reset_done, {'template_name':'registration/password_reset_done.html'}, name='password_reset_done'),
    path('reset/<uidb64>/<token>/', password_reset_confirm,
         {'template_name':'registration/password_reset_confirm.html','post_reset_redirect':'login:password_reset_complete'}, name='password_reset_confirm'),
    path('reset/done', password_reset_complete, {'template_name':'registration/password_reset_complete.html'}, name='password_reset_complete'),
]
