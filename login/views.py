from django.views.generic import CreateView

from django.contrib.auth.models import User
from .forms import RegistroForm

from django.urls import reverse_lazy

# Create your views here.

class RegistroUsuario(CreateView):
    model = User
    form_class = RegistroForm
    template_name = 'registro_usuario.html'
    success_url = reverse_lazy('login:login') 

    def get_context_data(self, **kwargs):
        title = "Registro de usuarios"
        context = super().get_context_data(**kwargs)
        context['title'] = title
        return context