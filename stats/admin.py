from django.contrib import admin
from .models import Estadistica

# Register your models here.
@admin.register(Estadistica)
class AdminEstadistica(admin.ModelAdmin):
    list_display = ('id', 'jugador', 'equipo', 'partido')
    list_filter = ('jugador', 'equipo', 'partido')