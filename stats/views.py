from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from django.contrib import messages
from django.db import IntegrityError

from .models import Estadistica
from .forms import EstadisticaForm
# Create your views here.

class EstadisticaCreate(CreateView):
    model = Estadistica
    form_class = EstadisticaForm
    template_name = 'estadistica_create.html'
    success_url = reverse_lazy('equipos:list')

    def get_context_data(self, **kwargs):
        title = "Nueva Estadística"
        context = super().get_context_data(**kwargs)
        context['title'] = title
        return context