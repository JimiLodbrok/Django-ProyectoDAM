from django.db import models
from equipos.models import Equipo
from jugadores.models import Jugador
from partidos.models import Partido

# Create your models here.

class Estadistica(models.Model):
    jugador = models.ForeignKey(Jugador, on_delete=models.CASCADE)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    partido = models.ForeignKey(Partido, on_delete=models.CASCADE)
    puntos = models.IntegerField(default=0)
    rebotes = models.IntegerField(default=0)
    asistencias = models.IntegerField(default=0)
    t2m = models.IntegerField(default=0)
    t2f = models.IntegerField(default=0)
    t3m = models.IntegerField(default=0)
    t3f = models.IntegerField(default=0)
    tlm = models.IntegerField(default=0)
    tlf = models.IntegerField(default=0)

    def __str__(self):
        return '{}-{}|{}'.format(self.equipo, self.jugador, self.partido)

    class Meta:
        ordering = ('id',)
        verbose_name_plural = 'Estadisicas'
        unique_together = ('jugador', 'partido', 'equipo')
