'''Estadisticas Configuration'''
from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views


urlpatterns = [
    path('new', login_required(views.EstadisticaCreate.as_view()), name="new"),
]