from django import forms
from .models import Estadistica
from material import *

class EstadisticaForm(forms.ModelForm):
    layout = Layout('jugador',
                    Row('equipo', 'partido'),
                    Row('puntos', 'rebotes', 'asistencias'),
                    Row('t2m', 't2f', 'tlm', 'tlf', 't3m', 't3f')
                    )
                   
    class Meta:
        model = Estadistica
        fields = '__all__'
        labels = {
            't2m': 'Tiros de 2 anotados',
            't2f': 'Tiros de 2 fallados',
            't3m': 'Tiros de 3 anotados',
            't3f': 'Tiros de 3 fallados',
            'tlm': 'Tiros libres anotados',
            'tlf': 'Tiros libres fallados',
        }  
        #sirve para poner el titulo de cada input del formulario
        #widgets = {'nombre': forms.textInput(attrs={'class': 'form-control'}), forms.Select().....} para definir el tipo de input